# GitKit Activities on Runestone Academy

## Editing

* Use GitPod with its VS Code in-browser editor.
* source/main.ptx is the entrypoint.
* Docs: https://pretextbook.org/doc/guide/html/guide-toc.html

## Building

```bash
pretext build
```

## Viewing/Previewing/Manual Testing

1. Click `go live` in bottom right toolbar in VS Code.
2. Navigate to `output/web`

If you rebuild, refresh this window.

## Publishing to Runestone Academy

We'll worry about that when we are farther along. Before we do, we need to

* Get exercise labels "right"
* Get xml:id's "right"
* Get structure "right"
* Get exercises "right"
